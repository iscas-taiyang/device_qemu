# qemu-ohos-riscv64-virt
# 说明 
目前qemu-ohos-riscv64-mini和qemu-ohos-riscv64-headless已完成适配（profile组件未适配，暂时做屏蔽处理），经测试系统正常启动可进入控制台，
编译及启动qemu-for-riscv64方法如下。

# 注册gitee账号和SSH公匙，并安装repo工具，可[参考网址](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-sourcecode.md)

# 拉取代码
```
repo init -u https://gitee.com/iscas-taiyang/manifest.git -b OpenHarmony-3.2-Beta2 -m ohos-rv64.xml --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
# 加载预安装文件
```
bash build/prebuilts_download.sh
```

# 工具链
```
#llvm-clang路径： prebuilts/gcc/linux-x86/llvm
#下载地址https://isrc.iscas.ac.cn/download/riscv/llvm12_rv64.tar.gz
#下载之后直接替换prebuilts/clang/ohos/linux-x86_64/llvm
```
# 加载openharmony 官方 docker环境
```
docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.8
docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.8
```
# 编译
```
./build.sh --product-name qemu-riscv64-linux-min
./build.sh --product-name qemu-riscv64-linux-headless
```

# qemu主机安装依赖组件
```
#libsdl2-dev graphical user interface
#gtk3-devel  UI 
#vte-devel serial/console GTK  
#bridge-utils virbr0 192.168.122.1
sudo apt-get install -y libaio-dev libbluetooth-dev libcapstone-dev libbrlapi-dev libbz2-dev
sudo apt-get install -y libcap-ng-dev libcurl4-gnutls-dev libgtk-3-dev
sudo apt-get install -y libibverbs-dev libjpeg8-dev libncurses5-dev libnuma-dev 
sudo apt-get install -y librbd-dev librdmacm-dev
sudo apt-get install -y libsasl2-dev libsdl2-dev libseccomp-dev libsnappy-dev libssh-dev 
sudo apt-get install -y libvde-dev libvdeplug-dev libvte-2.91-dev libxen-dev liblzo2-dev 
sudo apt-get install -y valgrind xfslibs-dev libiscsi-dev libnfs-dev
sudo apt-get install -y libvirt-daemon
sudo apt-get install -y bridge-utils virtinst virt-manager 
```
# qemu源码安装
```
wget https://download.qemu.org/qemu-7.0.0.tar.xz
tar -xvf qemu-7.0.0.tar.xz
cd qemu-7.0.0
mkdir build
cd build
../configure --prefix=/usr/local --sysconfdir=/etc --target-list=arm-softmmu,riscv64-softmmu,riscv32-softmmu  --audio-drv-list="sdl,pa,alsa"
make -j8
sudo make install
```
# qemu运行参考参数（不启动ramdisk）
```
# 如网络未通，可暂时去掉append参数ip=dhcp以及netdev整行参数
cd out/qemu-riscv64-linux/packages/phone/images
sudo qemu-system-riscv64 -machine virt -cpu rv64 -m 1024 -smp 2 \
-nographic \
-drive if=none,file=./updater.img,format=raw,id=updater,index=3 -device virtio-blk-device,drive=updater \
-drive if=none,file=./system.img,format=raw,id=system,index=2 -device virtio-blk-device,drive=system \
-drive if=none,file=./vendor.img,format=raw,id=vendor,index=1 -device virtio-blk-device,drive=vendor \
-drive if=none,file=./userdata.img,format=raw,id=userdata,index=0 -device virtio-blk-device,drive=userdata  \
-netdev bridge,id=net0,br=virbr0 -device virtio-net-device,netdev=net0,mac=12:22:33:44:55:66 \
-append "ip=dhcp loglevel=4 console=ttyS0,115200 init=init root=/dev/vdb rootfstype=ext4 rw rootwait ohos.boot.hardware=qemu.riscv64.linux default_boot_device=10007000.virtio_mmio sn=8823456789" \
-kernel Image
```
# qemu运行参考参数（启动ramdisk）
```
sudo qemu-system-riscv64 -machine virt -m 1024 -smp 2 \
-nographic \
-drive if=none,file=./updater.img,format=raw,id=updater,index=3 -device virtio-blk-device,drive=updater \
-drive if=none,file=./system.img,format=raw,id=system,index=2 -device virtio-blk-device,drive=system \
-drive if=none,file=./vendor.img,format=raw,id=vendor,index=1 -device virtio-blk-device,drive=vendor \
-drive if=none,file=./userdata.img,format=raw,id=userdata,index=0 -device virtio-blk-device,drive=userdata  \
-append "loglevel=4 console=ttyS0,115200 init=init root=/dev/ram0 rw rootwait ohos.boot.hardware=qemu.riscv64.linux default_boot_device=10007000.virtio_mmio sn=8823456789" \
-kernel Image \
-initrd ramdisk.img
```
# bridage（可选项）
```
QEMU_LIBEXECDIR=$(cat meson-private/cmd_line.txt | grep libexecdir | sed 's/= /\n/g' | sed  -n '2p')
sudo chmod +s $QEMU_LIBEXECDIR/qemu-bridge-helper
QEMU_SYSCONFIGDIR=$(cat meson-private/cmd_line.txt | grep sysconfdir | sed 's/= /\n/g' | sed  -n '2p')
sudo mkdir $QEMU_SYSCONFIGDIR/qemu
echo 'allow all' | sudo tee -a $QEMU_SYSCONFIGDIR/qemu/bridge.conf
```

# 待解决的一些问题
```
min
0 ramdisk重复启动（已暂时禁用，改为一次性启动）
1 softbus_server服务异常
2 storage_daemon服务异常
3 hisysevent日志异常
headless
4 部件异常
hiviewdfx_hilog_native 实际大小:0KB
idl_tool               实际大小:0KB
```

