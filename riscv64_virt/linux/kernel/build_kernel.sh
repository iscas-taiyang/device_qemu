#!/bin/bash

# Copyright (c) 2022  iscas-riscv-oh Open Source Organization .

set -e

pushd ${1}

export OHOS_SOURCE_ROOT=${2}
export OHOS_OUT_DIR=${OHOS_SOURCE_ROOT}/out
export OHOS_KERNEL_OBJ=${OHOS_OUT_DIR}/KERNEL_OBJ
export OHOS_IMAGES_DIR=${3}

export KERNEL_SOURCE_DIR=${4}
export KERNEL_CONFIG_DIR=${5}
export KERNEL_CONFIG_NAME=defconfig

#starfive/visionfive/hdf_config/khdf/Makefil 
#$(OUT_DIR)/kernel/OBJ/${KERNEL_VERSION}/drivers/hdf/khdf/hc_gen_build/hc-gen
export OUT_DIR=${OHOS_SOURCE_ROOT}/out
KERNEL_VERSION=riscv64_virt

export KERNEL_TEMP_DIR=${OHOS_KERNEL_OBJ}/kernel/src_tmp/${KERNEL_VERSION}

export PATH=${OHOS_SOURCE_ROOT}/prebuilts/clang/ohos/linux-x86_64/llvm/bin:$PATH
export PATH=${OHOS_SOURCE_ROOT}/prebuilts/gcc/linux-x86/riscv64/riscv64-unknown-linux-gnu/bin:$PATH
export MAKE_OPTIONS="ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- CC=clang"

#export PRODUCT_PATH=vendor/starfive/visionfive
export PRODUCT_PATH=vendor/hisilicon/hispark_taurus_standard

function copy_kernel(){
    if [ ! -d "${KERNEL_TEMP_DIR}" ];then
        mkdir -p ${OHOS_KERNEL_OBJ}/kernel/src_tmp
        cp -r ${KERNEL_SOURCE_DIR}  ${KERNEL_TEMP_DIR}
        ${OHOS_SOURCE_ROOT}/drivers/hdf_core/adapter/khdf/linux/patch_hdf.sh \
        ${OHOS_SOURCE_ROOT} \
        ${KERNEL_TEMP_DIR} \
        ${KERNEL_CONFIG_DIR}/hdf.patch
        make  ${MAKE_OPTIONS} -C ${KERNEL_TEMP_DIR} ${KERNEL_CONFIG_NAME}
        cp ${KERNEL_CONFIG_DIR}/${KERNEL_VERSION}.config ${KERNEL_TEMP_DIR}/.config
    fi
}

if [ ! -f "${OHOS_IMAGES_DIR}/Image" ];then
    copy_kernel
    make  ${MAKE_OPTIONS} -C ${KERNEL_TEMP_DIR} -j8
    mkdir -p ${OHOS_IMAGES_DIR}
    cp ${KERNEL_TEMP_DIR}/arch/riscv/boot/Image ${OHOS_IMAGES_DIR}/Image
fi
popd
